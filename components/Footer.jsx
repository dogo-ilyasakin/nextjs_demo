import React, { useContext } from 'react';
import Paper from '@material-ui/core/Paper';
import Link from 'next/link';
import { withTranslation } from '../i18n';

const Footer = ({ t }) => {
  return (
    <Paper
      style={{
        width: '100%',
        backgroundColor: '#3f51b5',
        borderRadius: 0,
      }}
    >
      <span style={{ color: '#fff', padding: 10, display: 'block' }}>
        {t('footer-title')}
      </span>
      <Link href="/topic/test">
        <span
          style={{
            display: 'block',
            marginTop: 10,
            padding: 10,
            cursor: 'pointer',
          }}
        >
          {t('static-test')}
        </span>
      </Link>
    </Paper>
  );
};

export default withTranslation('footer')(Footer);

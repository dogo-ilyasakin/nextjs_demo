import React, { useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import NavButton from '../components/NavButton';
import Link from 'next/link';
import CartContext from '../context/CartContext';
import { useRouter } from 'next/router';
import PageType from '../context/PageTypeContext';
import axios from 'axios';
import CurrentProduct from '../context/CurrentProductContext';
import { withTranslation } from '../i18n';
import UserContext from '../context/UserContext';

const Header = ({ t }) => {
  const { cart } = useContext(CartContext);
  const { pageType } = useContext(PageType);
  const { currentProduct, setCurrentProduct } = useContext(CurrentProduct);
  const { user, setUser } = useContext(UserContext);
  const router = useRouter();

  const changeLocale = (locale) => {
    if (pageType === 'HOMEPAGE') {
      router.push(
        {
          pathname: router.pathname,
          query: router.query,
        },
        {
          pathname: router.pathname,
          query: router.query,
        },
        { locale }
      );
    }

    if (pageType === 'PRODUCT') {
      const fetchData = async () => {
        const response = await axios.post('http://localhost:3000/api/product', {
          id: currentProduct.id,
          locale,
        });

        router.push(
          {
            pathname: router.pathname,
            query: { slug: response.data.slug },
          },
          {
            pathname: router.pathname,
            query: { slug: response.data.slug },
          },
          { locale }
        );
      };

      fetchData();
    }
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <Link href="/">
          <Typography variant="h6" style={{ cursor: 'pointer' }}>
            Next Demo
          </Typography>
        </Link>
        <div style={{ marginLeft: 'auto' }}>
          <NavButton
            properties={{
              name: t('language'),
              items: [
                {
                  title: t('language-english'),
                  onClick: () => changeLocale('en'),
                },
                {
                  title: t('language-turkish'),
                  onClick: () => changeLocale('tr'),
                },
              ],
            }}
          />
          <NavButton
            properties={{
              name: t('profile'),
              items: user.isLoggedIn
                ? [
                    {
                      title: 'İlyas Akın',
                    },
                    { title: t('account') },
                    { title: t('admin') },
                    {
                      title: t('logout'),
                      onClick: () => {
                        setUser({ isLoggedIn: false });
                      },
                    },
                  ]
                : [
                    {
                      title: t('guest'),
                    },
                    {
                      title: t('login'),
                      onClick: () => {
                        setUser({ isLoggedIn: true });
                      },
                    },
                  ],
            }}
          />
          <NavButton
            style={{ fontWeight: 700 }}
            properties={{
              name: `${t('cart')}`,
              items: cart.cartItems,
            }}
          />
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default withTranslation('header')(Header);

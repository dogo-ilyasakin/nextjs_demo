import React, { useState } from 'react';
import Container from '@material-ui/core/Container';
import Footer from './Footer';
import Header from './Header';

const Layout = ({ children }) => {
  const mockAvailableLanguages = ['Turkish', 'English', 'French'];

  return (
    <Container disableGutters style={{ minHeight: '100vh' }}>
      <Header languages={mockAvailableLanguages} />
      {children}
      <Footer />
    </Container>
  );
};

export default Layout;

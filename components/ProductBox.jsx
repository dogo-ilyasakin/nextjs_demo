import React from 'react';
import Paper from '@material-ui/core/Paper';
import Link from 'next/link';
import Button from '@material-ui/core/Button';

const ProductBox = ({ product }) => {
  return (
    <Paper style={{ margin: '20px' }}>
      <div style={{ height: 400, width: '100%' }}>
        <img style={{ width: '100%', padding: 100 }} src={product.image} />
      </div>
      <h3 style={{ textAlign: 'center' }}>{product.title}</h3>
      <h4 style={{ textAlign: 'center' }}>
        {'$'}
        {product.price}
      </h4>
      <div style={{ display: 'flex', textAlign: 'center' }}>
        <div style={{ flex: '1 1 0px', marginBottom: '0.3rem' }}>
          <Link href={`/product/${product.slug}`}>
            <Button variant="contained">STATIC</Button>
          </Link>
        </div>
        <div style={{ flex: '1 1 0px' }}>
          <Link href={`/product-ssr/${product.slug}`}>
            <Button variant="contained">SSR</Button>
          </Link>
        </div>
      </div>
    </Paper>
  );
};

export default ProductBox;

import React from 'react';
import Paper from '@material-ui/core/Paper';
import ProductBox from './ProductBox';

const ProductList = ({ products }) => {
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: 'repeat(3, 1fr)',
      }}
    >
      {products.map((product) => {
        return (
          <ProductBox key={`product_id_${product.id}`} product={product} />
        );
      })}
    </div>
  );
};

export default ProductList;

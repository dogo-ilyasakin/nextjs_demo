import { createContext } from 'react';

const Locale = createContext({});

export default Locale;

import { createContext } from 'react';

const PageType = createContext({});

export default PageType;

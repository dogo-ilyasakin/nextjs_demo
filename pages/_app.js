import '../styles/globals.css';
import { default as CartContext } from '../context/CartContext';
import { default as ProductsContext } from '../context/ProductsContext';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import PageType from '../context/PageTypeContext';
import CurrentProduct from '../context/CurrentProductContext';
import User from '../context/UserContext';
import { i18n, appWithTranslation } from '../i18n';

function MyApp({ Component, pageProps }) {
  const [pageType, setPageType] = useState(null);
  const [user, setUser] = useState({ isLoggedIn: false });
  const [products, setProducts] = useState([]);
  const [currentProduct, setCurrentProduct] = useState(null);
  const { locale } = useRouter();
  const [cart, setCart] = useState({ cartItems: [], locale });

  useEffect(() => {
    const LOCAL_CART = localStorage.getItem('CART');
    if (LOCAL_CART) setCart(JSON.parse(LOCAL_CART));
  }, []);

  useEffect(() => {
    localStorage.setItem('CART', JSON.stringify(cart));
  }, [cart, setCart]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await axios.post('http://localhost:3000/api/products', {
        locale,
      });
      setProducts(response.data);
    };

    fetchData();

    i18n.changeLanguage(locale);

    if (cart.cartItems && cart.locale !== locale) {
      const newCart = { cartItems: [], locale };
      cart.cartItems.forEach((cartItem) => {
        axios
          .post('http://localhost:3000/api/product', {
            id: cartItem.id,
            locale,
          })
          .then((response) => {
            newCart.cartItems.push(response.data);
            setCart(newCart);
          });
      });
    }
  }, [locale]);

  return (
    <User.Provider value={{ user, setUser }}>
      <PageType.Provider value={{ pageType, setPageType }}>
        <ProductsContext.Provider value={{ products, setProducts }}>
          <CartContext.Provider value={{ cart, setCart }}>
            <CurrentProduct.Provider
              value={{ currentProduct, setCurrentProduct }}
            >
              <Component {...pageProps} />
            </CurrentProduct.Provider>
          </CartContext.Provider>
        </ProductsContext.Provider>
      </PageType.Provider>
    </User.Provider>
  );
}

export default appWithTranslation(MyApp);

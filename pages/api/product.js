import fs from 'fs';

export default (req, res) => {
  if (req.method === 'POST') {
    const { locale, id, slug } = req.body;

    let products = fs.readFileSync(
      `${process.cwd()}/json/products/${locale}.json`,
      {
        encoding: 'utf-8',
      }
    );

    products = JSON.parse(products);

    let product;

    if (slug) {
      product = products.find((product) => product.slug === slug);
      if (product) res.status(200).json(product);
      if (!product) res.status(404).json({ error: 'Some shit happened' });
    } else if (id) {
      product = products.find((product) => product.id === id);
      if (id) res.status(200).json(product);
      if (id === undefined)
        res.status(404).json({ error: 'Some shit happened' });
    }
  }
};

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import fs from 'fs';

export default (req, res) => {
  if (req.method === 'POST') {
    const { locale } = req.body;
    const readable = fs.createReadStream(
      `${process.cwd()}/json/products/${locale}.json`
    );
    readable.pipe(res);
    res.statusCode = 200;
  }

  if (req.method === 'GET') {
    res.send(req.params);
  }
};

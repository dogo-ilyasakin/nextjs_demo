import styles from '../styles/Home.module.css';
import React, { useContext, useEffect } from 'react';
import Paper from '@material-ui/core/Paper';
import ProductList from '../components/ProductList';
import Layout from '../components/Layout';
import ProductContext from '../context/ProductsContext';
import PageType from '../context/PageTypeContext';
import CurrentProduct from '../context/CurrentProductContext';

export default function Home() {
  const { setPageType } = useContext(PageType);
  const { products } = useContext(ProductContext);

  useEffect(() => {
    setPageType('HOMEPAGE');
    return () => setPageType(null);
  }, []);

  return (
    <Layout>
      <Paper style={{ borderRadius: 0 }}>
        <ProductList products={products} />
      </Paper>
    </Layout>
  );
}

import React from 'react';
import Layout from '../../components/Layout';
import { Button } from '@material-ui/core';
import axios from 'axios';
import { useContext, useEffect } from 'react';
import PageType from '../../context/PageTypeContext';
import CurrentProduct from '../../context/CurrentProductContext';
import { withTranslation } from '../../i18n';
import CartContext from '../../context/CartContext';
import { useRouter } from 'next/router';

const Product = ({ product, t }) => {
  const { locale } = useRouter();
  const { cart, setCart } = useContext(CartContext);
  const { setPageType } = useContext(PageType);
  const { setCurrentProduct } = useContext(CurrentProduct);

  useEffect(() => {
    setCurrentProduct(product);
    return () => setCurrentProduct(null);
  }, []);

  useEffect(() => {
    setPageType('PRODUCT');
    return () => setPageType(null);
  }, []);

  return (
    <Layout>
      <div style={{ width: '100%', display: 'flex' }}>
        <img src={product?.image} style={{ width: '60%', padding: 150 }} />
        <div style={{ width: '40%', padding: 30 }}>
          <h4 style={{ margin: 0 }}>{product?.title}</h4>
          <h5 style={{ marginTop: 5 }}>
            {t('product-id')}: {product?.id}
          </h5>
          <h5>
            {t('price')}: {`$${product?.price}`}
          </h5>
          <h6>{product?.description}</h6>
          <Button
            variant="contained"
            color="primary"
            onClick={() =>
              setCart({ cartItems: [...cart.cartItems, product], locale })
            }
          >
            {t('add-to-cart')}
          </Button>
        </div>
      </div>
    </Layout>
  );
};

export async function getServerSideProps({ locale, query }) {
  const response = await axios.post(`http://localhost:3000/api/product/`, {
    locale,
    slug: query.slug,
  });

  return {
    props: { product: response.data },
  };
}

export default withTranslation('product-details')(Product);

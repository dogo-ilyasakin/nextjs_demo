import React, { useContext, useEffect } from 'react';
import Layout from '../../components/Layout';
import { Button } from '@material-ui/core';
import CartContext from '../../context/CartContext';
import axios from 'axios';
import DefaultErrorPage from 'next/error';
import { useRouter } from 'next/router';
import { RotateSpinner } from 'react-spinners-kit';
import PageType from '../../context/PageTypeContext';
import CurrentProduct from '../../context/CurrentProductContext';
import { withTranslation } from '../../i18n';

const ProductDetails = ({ product, t }) => {
  const { cart, setCart } = useContext(CartContext);
  const { setPageType } = useContext(PageType);
  const { setCurrentProduct } = useContext(CurrentProduct);
  const router = useRouter();
  const { locale } = router;

  if (router.isFallback) {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          height: '100vh',
        }}
      >
        <RotateSpinner color="#000" />
      </div>
    );
  }

  if (Object.keys(product).length === 0) {
    return <DefaultErrorPage statusCode={404}></DefaultErrorPage>;
  }

  useEffect(() => {
    setCurrentProduct(product);
    return () => setCurrentProduct(null);
  }, []);

  useEffect(() => {
    setPageType('PRODUCT');
    return () => setPageType(null);
  }, []);

  return (
    <Layout>
      <div style={{ width: '100%', display: 'flex' }}>
        <img src={product?.image} style={{ width: '60%', padding: 150 }} />
        <div style={{ width: '40%', padding: 30 }}>
          <h4 style={{ margin: 0 }}>{product?.title}</h4>
          <h5 style={{ marginTop: 5 }}>
            {t('product-id')}: {product?.id}
          </h5>
          <h5>
            {t('price')}: {`$${product?.price}`}
          </h5>
          <h6>{product?.description}</h6>
          <Button
            variant="contained"
            color="primary"
            onClick={() =>
              setCart({ cartItems: [...cart.cartItems, product], locale })
            }
          >
            {t('add-to-cart')}
          </Button>
        </div>
      </div>
    </Layout>
  );
};

export async function getStaticPaths({ locales }) {
  const paths = [];

  await Promise.all(
    locales.map(async (locale) => {
      const response = await axios.post('http://localhost:3000/api/products', {
        locale,
      });
      response.data.forEach((product) => {
        paths.push({
          params: { slug: product.slug },
          locale,
        });
      });
    })
  );

  return { paths, fallback: true };
}

export async function getStaticProps({ params, locale }) {
  try {
    const response = await axios.post(`http://localhost:3000/api/product/`, {
      locale,
      slug: params.slug,
    });
    return { props: { product: response.data } };
  } catch {
    return { props: { product: {} } };
  }
}

export default withTranslation('product-details')(ProductDetails);

import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import Paper from '@material-ui/core/Paper';
import Layout from '../../components/Layout';
// import axios from 'axios';

const Topic = () => {
  const router = useRouter();
  const { tid } = router.query;

  // useEffect(() => {
  //   axios.get()
  // }, []);

  return (
    <Layout>
      <Paper style={{ borderRadius: 0, padding: 15 }}>
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem
        accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
        illo inventore veritatis et quasi architecto beatae vitae dicta sunt
        explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
        odit aut fugit, sed quia consequuntur magni dolores eos qui ratione
        voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum
        quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam
        eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
        voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam
        corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
        Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse
        quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo
        voluptas nulla pariatur?
      </Paper>
    </Layout>
  );
};

export default Topic;
